<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title></title>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
<?php
include 'include/header.php';
?>
  <div class="content">

          <?php include 'include/breadcrumb.php'; ?>

    <section class="container">
      <div class="row work-type">
          <div class="col-12">
              <h3>Вид работ № 1</h3>
              <p>Описание работы. Резка проводов. Разработка и производство изделий. Описание работы. Резка проводов. Разработка и производство изделий. Описание работы. Резка проводов. Разработка и производство изделий. </p>
              <p>Описание работы. Резка проводов. Разработка и производство изделий. Описание работы. Резка проводов. Разработка и производство изделий. </p>
              <figure>
                <img src="img/3.jpg" alt="провода">
              </figure>
              <p>Описание работы. Резка проводов. Разработка и производство изделий. Описание работы. Резка проводов. Разработка и производство изделий. </p>
              <p>Описание работы. Резка проводов. Разработка и производство изделий. </p>
              <table class="table">
                <caption>Цена за штуку в зависимости от объема работ</caption>
                <thead>
                  <tr>
                    <th>тип \ шт</th>
                    <th>до 500</th>
                    <th>500 - 1000</th>
                    <th>более 1000</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>0.2</th>
                    <td>2.5</td>
                    <td>2.3</td>
                    <td>2.0</td>
                  </tr>
                  <tr>
                    <th>0.3</th>
                    <td>3.0</td>
                    <td>2.7</td>
                    <td>2.5</td>
                  </tr>
                  <tr>
                    <th>0.5</th>
                    <td>4.5</td>
                    <td>4.2</td>
                    <td>3.9</td>
                  </tr>
                </tbody>
              </table>
          </div>
      </div>
    </section>
  </div>
  <?php include_once 'include/footer.php'; ?>
</body>

</html>