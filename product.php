<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include 'include/header.php';
    ?>
    <div class="content">
    
    <?php include 'include/breadcrumb.php'; ?>

        <section class="container">
            <div class="row prod-item">
                <div class="col-12">
                    <h3>Product name</h3>
                </div>
                <div class="col-12 col-md-4">
                    <div class="prod-item-img-cont">
                        <img src="img/2.jpg" alt="" />
                    </div>
                    <p>
                        Description. Some basic facts about product. Two or three
                        sentences. Description. Some basic facts about product. Two or
                        three sentences. Description. Some basic facts about product.
                        Two or three sentences.
                    </p>
                    <span class="prod-item-price">
                        <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                            class="prod-price-currency">рублей</span>
                    </span>
                    <a href="" role="button" class="prod-item-buy-btn">Купить</a>
                </div>
                <div class="col-12 col-md-8">
                    <ul class="nav nav-tabs" id="prod-item-params-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="prod-item-params-tab-1" data-toggle="tab"
                                href="#prod-item-params-1" role="tab" aria-controls="prod-item-params-1"
                                aria-selected="true">Params 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="prod-item-params-tab-2" data-toggle="tab" href="#prod-item-params-2"
                                role="tab" aria-controls="prod-item-params-2" aria-selected="false">Params 2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="prod-item-params-tab-3" data-toggle="tab" href="#prod-item-params-3"
                                role="tab" aria-controls="prod-item-params-3" aria-selected="false">Params 3</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="prod-item-param">
                        <div class="tab-pane fade show active" id="prod-item-params-1" role="tabpanel"
                            aria-labelledby="prod-item-params-1-tab">
                            <h4 class="prod-item-tab-params-heading">Конфигурация</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные
                                        устройства Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не более
                                        15 мА</span>
                                </li>
                            </ul>
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не
                                        более 15
                                        мА</span>
                                </li>
                            </ul>
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные
                                        устройства Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="prod-item-params-2" role="tabpanel"
                            aria-labelledby="prod-item-params-2-tab">
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные устройства
                                        Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не
                                        более 15
                                        мА</span>
                                </li>
                            </ul>
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные устройства
                                        Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не
                                        более 15
                                        мА</span>
                                </li>
                            </ul>
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные устройства
                                        Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="prod-item-params-3" role="tabpanel"
                            aria-labelledby="prod-item-params-3-tab">
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не
                                        более 15
                                        мА</span>
                                </li>
                            </ul>
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные устройства
                                        Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не
                                        более 15
                                        мА</span>
                                </li>
                            </ul>
                            <ul class="list-group">
                                <li>
                                    <span>Количество подключаемых источников</span><span>:&nbsp;</span><span>2 и 4
                                        канала записи
                                    </span>
                                </li>
                                <li>
                                    <span>Подключение</span><span>:&nbsp;</span><span>контакт под винт (микрофоны и
                                        телефоннын
                                        линиии)</span>
                                </li>
                                <li>
                                    <span>Тип подключаемого источника звука</span><span>:&nbsp;</span><span>микрофон,
                                        телефон,
                                        радиостанция, переговорные устройства
                                        Клиент-Кассир, генераторы сигналов.</span>
                                </li>
                                <li>
                                    <span>Максимальное количество устройств</span><span>:&nbsp;</span><span>нет
                                        ограничений</span>
                                </li>
                            </ul>
                            <h4 class="prod-item-tab-params-heading">Поддерживаемые микрофоны</h4>
                            <ul class="list-group">
                                <li>
                                    <span>Тип микрофонов</span><span>:&nbsp;</span><span>2-х и 3-х проводные</span>
                                </li>
                                <li>
                                    <span>Питание микрофонов</span><span>:&nbsp;</span><span>от устройства, при условии
                                        токопотреблении не
                                        более 15
                                        мА</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include_once 'include/footer.php'; ?>
</body>

</html>