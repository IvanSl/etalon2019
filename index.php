<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title></title>
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery-3.4.1.min.js"></script>
  <script src="js/sort.js"></script>
</head>

<body>
  <?php
include 'include/header.php';
?>
  <div class="content">
    <div class="section-heading">

      <div class="container">
        <div class="row">
          <div class="col-12">
            <h4>Выполняемые нами работы</h4>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="prod-item col-12 col-sm-6 col-lg-3">
          <a href="work-type.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Корпуса под заказ</h5>
          <div class="prod-item-img-cont">
            <img src="img/3.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description.
          </p>
        </div>
        <div class="prod-item col-12 col-sm-6 col-lg-3">
          <a href="work-type.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Резка проводов</h5>
          <div class="prod-item-img-cont">
            <img src="img/3.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description. Some basic facts about product.
            Two or three sentences.
          </p>
        </div>
        <div class="prod-item col-12 col-sm-6 col-lg-3">
          <a href="work-type.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Системы мониторинга</h5>
          <div class="prod-item-img-cont">
            <img src="img/3.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description.
          </p>
        </div>
      </div>
    </div>
    <div class="section-heading">

      <div class="container">
        <div class="row">
          <div class="col-12">
            <h4>Аудиорегистраторы</h4>
          </div>
        </div>
      </div>
    </div>
    <nav id="filter">
      <div class="container">
        <div class="row">
          <div class="col-3">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="filter-channels" id="filter-channels-1" value="1"
                checked>
              <label class="form-check-label" for="filter-channels-1">
                1 канал
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="filter-channels" id="filter-channels-2" value="2">
              <label class="form-check-label" for="filter-channels-2">
                2 канала
              </label>
            </div>
            <div class="form-check disabled">
              <input class="form-check-input" type="radio" name="filter-channels" id="filter-channels-4" value="4">
              <label class="form-check-label" for="filter-channels-4">
                4 канала
              </label>
            </div>
          </div>
          <div class="col-3">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="filter-param-group-2" id="filter-param-group-2-1"
                value="1" checked>
              <label class="form-check-label" for="filter-param-group-2-1">
                param 2 = 1
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="filter-param-group-2" id="filter-param-group-2-2"
                value="2">
              <label class="form-check-label" for="filter-param-group-2-2">
                param 2 = 2
              </label>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <section id="prod-items-container" class="container">
      <div class="row">

        <div class="prod-item col-12 col-sm-6 col-lg-3" filter-channels="1" filter-param-group-2="1">
          <div class="prod-item-bg-cont"></div>
          <a href="product.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Product name</h5>
          <div class="prod-item-img-cont">
            <img src="img/2.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description. Some basic facts about product.
            Two or three sentences.
          </p>
          <div class="prod-item-bottom-cont">
            <span class="prod-item-price">
              <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                class="prod-price-currency">рублей</span>
            </span>
            <a href="" role="button" class="prod-item-buy-btn">Купить</a>
            <p class="prod-item-tags">
              <span>1 канал</span>
              <span>параметр 2 = 1</span>
            </p>
          </div>
        </div>

        <div class="prod-item col-12 col-sm-6 col-lg-3" filter-channels="2" filter-param-group-2="2">
          <div class="prod-item-bg-cont"></div>
          <a href="product.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Product name</h5>
          <div class="prod-item-img-cont">
            <img src="img/2.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description. Some basic facts about product.
            Two or three sentences.
          </p>
          <div class="prod-item-bottom-cont">
            <span class="prod-item-price">
              <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                class="prod-price-currency">рублей</span>
            </span>
            <a href="" role="button" class="prod-item-buy-btn">Купить</a>
            <p class="prod-item-tags">
              <span>2 канала</span>
              <span>параметр 2 = 2</span>
            </p>
          </div>
        </div>

        <div class="prod-item col-12 col-sm-6 col-lg-3" filter-channels="4" filter-param-group-2="2">
          <div class="prod-item-bg-cont"></div>
          <a href="product.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Product name</h5>
          <div class="prod-item-img-cont">
            <img src="img/2.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description. Some basic facts about product.
            Two or three sentences.
          </p>
          <div class="prod-item-bottom-cont">
            <span class="prod-item-price">
              <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                class="prod-price-currency">рублей</span>
            </span>
            <a href="" role="button" class="prod-item-buy-btn">Купить</a>
            <p class="prod-item-tags">
              <span>4 канала</span>
              <span>параметр 2 = 2</span>
            </p>
          </div>
        </div>

        <div class="prod-item col-12 col-sm-6 col-lg-3" filter-channels="2" filter-param-group-2="1">
          <div class="prod-item-bg-cont"></div>
          <a href="product.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Product name</h5>
          <div class="prod-item-img-cont">
            <img src="img/2.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description. Some basic facts about product.
            Two or three sentences.
          </p>
          <div class="prod-item-bottom-cont">
            <span class="prod-item-price">
              <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                class="prod-price-currency">рублей</span>
            </span>
            <a href="" role="button" class="prod-item-buy-btn">Купить</a>
            <p class="prod-item-tags">
              <span>2 канала</span>
              <span>параметр 2 = 1</span>
            </p>
          </div>
        </div>

        <div class="prod-item col-12 col-sm-6 col-lg-3" filter-channels="1" filter-param-group-2="2">
          <div class="prod-item-bg-cont"></div>
          <a href="product.php" class="stretched-link"></a>
          <h5 class="prod-item-name">Product name</h5>
          <div class="prod-item-img-cont">
            <img src="img/2.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description. Some basic facts about product.
            Two or three sentences.
          </p>
          <div class="prod-item-bottom-cont">
            <span class="prod-item-price">
              <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                class="prod-price-currency">рублей</span>
            </span>
            <a href="" role="button" class="prod-item-buy-btn">Купить</a>
            <p class="prod-item-tags">
              <span>1 канал</span>
              <span>параметр 2 = 2</span>
            </p>
          </div>
        </div>

        <div class="prod-item col-12 col-sm-6 col-lg-3" filter-channels="4" filter-param-group-2="1">
          <div class="prod-item-bg-cont"></div>
          <a href="product.html" class="stretched-link"></a>
          <h5 class="prod-item-name">Product name</h5>
          <div class="prod-item-img-cont">
            <img src="img/2.jpg" alt="" />
          </div>
          <p>
            Description. Some basic facts about product. Two or three
            sentences. Description. Some basic facts about product. Two or
            three sentences. Description.
          </p>
          <div class="prod-item-bottom-cont">
            <span class="prod-item-price">
              <span class="prod-item-price-value">12 000</span><span>&nbsp;</span><span
                class="prod-price-currency">рублей</span>
            </span>
            <a href="" role="button" class="prod-item-buy-btn">Купить</a>
            <p class="prod-item-tags">
              <span>4 канала</span>
              <span>параметр 2 = 1</span>
            </p>
          </div>
        </div>

      </div>
    </section>
  </div>
  <?php include_once 'include/footer.php'; ?>

</body>

</html>