<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title></title>
    <link rel="stylesheet" href="css/style.css">
    
    <script src="https://api-maps.yandex.ru/2.1/?apikey=b7c233b7-0e9b-457b-bd21-e32371bb27b9&lang=ru_RU" type="text/javascript">
    </script>
    <script src="js/contacts-map.js"></script>
</head>

<body>
    <?php
include 'include/header.php';
?>
    <div class="content">
        <div class="section-heading">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h4>Наши контакты</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <address>
                        Вы можете связаться с нами по электронной почте: <a
                            href="mailto:webmaster@somedomain.com"><b>test@test.rur</a></b>.<br>
                        Или найти нас по адресу: Москва, Туристская улица, 25
                    </address>
                </div>
                <div id="contacts-map-cont" class="col-12">
                    <div id="contacts-map" style="width: 100%; height: 400px" ></div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once 'include/footer.php'; ?>

</body>

</html>