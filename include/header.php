<header id="header" class="fixed-top">
    <div class="container">
      <div class="row">
        <div class="col-4 logo"><a href="index.php">ЭТАЛОН</a></div>
        <div class="col-8 tel-and-mail">
          <p>
            <a href="tel:+9636180850" class="tel"><span>+7</span>&nbsp;<span
                class="tel-prefix">(963)</span>&nbsp;<span>618-08-50</span></a>
          </p>
          <p>
            <a href="mailto:slojenikin2012@gmail.com">slojenikin2012@gmail.com</a>
          </p>
        </div>
      </div>
    </div>
  </header>