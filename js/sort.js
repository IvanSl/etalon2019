$(document).ready(function () {
  jQuery.fn.reverse = [].reverse;
  $('#filter input[type=radio]').on('change', function () {
    $('#prod-items-container .prod-item').each(function () {
      $(this).removeClass('temp-background');
    });
    $('#prod-items-container .prod-item[' + this.name + '=' + ($(this).attr('value')) + ']').reverse().each(function () {
      // TODO: инвертировать массив элементов перед вставкой
      $(this).detach().prependTo('#prod-items-container > .row').addClass('temp-background');
    });
  });
});