var myMap;
ymaps.ready(init);

    function init () {
        myMap = new ymaps.Map('contacts-map', {
            center: [55.855, 37.420], 
            zoom: 10,
            controls: ['smallMapDefaultSet']
        }, 
        {
            searchControlProvider: 'yandex#search'
        });
        var myPlacemark = new ymaps.Placemark([55.855021, 37.420328], {
            balloonContentHeader: '',
            balloonContentBody: '',
            balloonContentFooter: '',
            hintContent: ''
        });
        
        myMap.geoObjects.add(myPlacemark);
    }
 
 